from django.urls import path
from . import views

urlpatterns = [
    path("", views.recipe_list, name="home"),
    path("<int:id>", views.show_recipes, name="show_recipe"),
    path("create/", views.create_recipe, name="create_recipe"),
    path("<int:id>/edit/", views.edit_recipes, name="edit_recipe")
]
