from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe
from recipes.forms import RecipeForm

# Create your views here.
def show_recipes(request, id):
    recipe = get_object_or_404(Recipe, id=id)

    # return render(request, "recipes/detail.html")

    context = {
        "recipe_object": recipe
    }
    return render(request, "recipes/detail.html", context)

def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)

def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("home")
    else:
        form = RecipeForm()

    context = {
        "form": form,
        }

    return render(request, "recipes/create.html", context)

def edit_recipes(request, id):
    edit_recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=edit_recipe)
        if form.is_valid():
            form.save()
            return redirect("home")
    else:
        form = RecipeForm(instance=edit_recipe)

    context = {
        "edit" : edit_recipe,
        "form": form
    }

    return render(request, "recipes/edit.html", context)
